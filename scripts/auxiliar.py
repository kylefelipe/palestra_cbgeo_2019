PRODUCAO_FLORESTAL = {
    'PLANTIO' : ['STRING'], 
    'EXECUCAO' : ['STRING'],
    'ESPACAMENT' : ['STRING'],
    'AREA' : ['REAL'], 
    'CONDUCAO' : ['STRING'], 
    'ESPECIE' :['STRING'], 
    'ORIGEM' : ['STRING'], 
    'TECNICO' : ['STRING'], 
    'ART' : ['STRING'], 
    'IDENTIFICA' : ['STRING'],
    'CNPJ_CPF' : ['STRING', 'INTEGER'], 
    'QUANTO' : ['REAL'],
    'DATAPLANTI' : ['STRING', 'DATE'] }
 
PROPRIEDADE = {
    'MATRICULA' : ['STRING'], 
    'COMARCA' : ['STRING'], 
    'CARTORIO' : ['REAL'], 
    'LIVRO' : ['REAL'], 
    'MUNICIPIO' : ['STRING'],
    'PROPRIEDAD' : ['STRING'], 
    'DENOMINACA' : ['STRING'], 
    'PROPRIETAR' : ['STRING'], 
    'VINCULO' : ['STRING']
}

rel_erro = """
############################## RELATORIO DE ERROS ##############################

-------------------------------- DADOS CLIENTE ---------------------------------

NOME: {}

DOCUMENTO: {}              NÚMERO:  {}

---------------------------------- RESPONSÁVEL ---------------------------------

NOME: {}

------------------------------- INCONFORMIDADES --------------------------------

{}

--------------------------------------------------------------------------------
"""