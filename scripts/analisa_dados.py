from os.path import sep
from qgis.core import *
from qgis.core import (
    QgsProject, QgsVectorLayer
)
import qgis.utils
from busca_arquivos import *
from gera_relatorio_erros import *
from auxiliar import *

qgs = QgsApplication( [] , False )

qgs.initQgis()

projetoAnalise = QgsProject.instance()

diretorioBusca = "/home/kylefelipe/palestra_cbgeo_2019/dados/empresa_tal_1111111111111/21 ago aaaa/"

nome_cliente = str( input( "Digite o nome do cliente: " ) )
documento_cliente = str( input( "Digite o tipo de documento informado (CPF/CNPJ): " ) )
numero_documento_cliente = str( input( "Digite o número do documento do cliente: " ) )
tecnico_analise = str( input( "Digite o nome do Técnico da análise: " ) )

dados = [ nome_cliente.upper()
         , documento_cliente.upper()
         , numero_documento_cliente.upper()
         , tecnico_analise.upper() ]

def importaShp(caminho):
    """Import a Shapefile to project"""
    nomeCamada = str( caminho.split( sep )[ -1 ][ :-4 ] )
    # print( nomeCamada )
    shpLayer = QgsVectorLayer( caminho , nomeCamada , "ogr" )
    if shpLayer.isValid():
        projetoAnalise.instance().addMapLayer( shpLayer )
        # print( shpLayer.dataProvider().encoding() )
        shpLayer.setProviderEncoding('windows-1252')
    else:
        print( "Camada {} inválida!".format( caminho ) )

print( "Importando Camadas" )
for i in buscaArquivosShp(diretorioBusca ):
    importaShp( i )

camadasProjeto = QgsProject.instance().mapLayers()
nomesCamadasPropriedades = QgsProject.instance().mapLayersByName('PROPRIEDADE')
nomesCamadasProdFlore = QgsProject.instance().mapLayersByName('PRODUCAO_FLORESTAL')

erros_globai = {}

# verificando campos das tabelas

camposCamadas = []

print( "Verificando os campos das camadas." )
# Verificando os nomes dos campos das camadas
for layer in  nomesCamadasProdFlore:
    camposFaltantes = []
    camposCamada = [ field.name().upper() for field in layer.fields() ]
    for fieldObrigatorio in PRODUCAO_FLORESTAL.keys():
        if not fieldObrigatorio in camposCamada:
            camposFaltantes.append( fieldObrigatorio )
    if len( camposFaltantes ) > 0:
        erros_globai[ layer.id() ] = {"FALTA CAMPO" : camposFaltantes }
     
for layer in  nomesCamadasPropriedades:
    camposFaltantes = []
    camposCamada = [ field.name().upper() for field in layer.fields() ]
    for fieldObrigatorio in PROPRIEDADE.keys():
        if not fieldObrigatorio in camposCamada:
            camposFaltantes.append( fieldObrigatorio )
    if len( camposFaltantes ) > 0:
         erros_globai[ layer.id() ] = {"FALTA CAMPO" : camposFaltantes }

# Verificando local da Propriedade: São 5570 municipios no Brasil, 20 poligonos de propriedade = 111.400 iterações
caminhoGpkg = "/home/kylefelipe/palestra_cbgeo_2019/dados/bcim_2016_21_11_2018.gpkg"
municipios = QgsVectorLayer( caminhoGpkg + '|layername=lim_municipio_a' , "lim_municipio_a" , "ogr" )

for layer in nomesCamadasPropriedades:
    erros = {}
    for limite in layer.getFeatures():
        for municipio in municipios.getFeatures():
            if limite.geometry().intersects( municipio.geometry() ):
                if not limite[ 'Municipio' ].upper() == municipio[ 'nome' ].upper():
                    erros[ 'poligono ID ' + str( limite.id() ) ] = "Municipio da propriedade diverge do informado"
    if len( erros ) > 0:
        erros_globai[ "MUNICIPIO ERRADO" ] = {layer.id() : erros}

if len( erros_globai ) > 0:
    gera_arquivo( diretorioBusca , erros_globai )

montaRelatorioErro( dados , diretorioBusca )

# Salvando um arquivo de projeto dentro da pasta
projetoAnalise.write( diretorioBusca + 'analise.qgs')
print( "Salvando Projeto." )
qgs.exitQgis()
print( "Fim de análise! \nVIDA LONGA E PROSPERA!!!")