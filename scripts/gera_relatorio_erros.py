import os
import json
from auxiliar import rel_erro

def gera_arquivo(diretorio, erro):
    """GErando aquivo json de erro"""
    if os.path.isdir(diretorio):
        if os.path.isfile( diretorio  + 'arquivo_erro.json'):
            with open(diretorio + 'arquivo_erro.json', 'a') as file:
                file.write( json.dumps( erro , indent=4 ) )
        else:
            with open(diretorio + 'arquivo_erro.json', 'w') as file:
                file.write( json.dumps( erro, indent=4 ) )


def montaRelatorioErro(dados, diretorioBusca):
    """Montando o arquivo txt do relatório de erros"""

    
    nome_cliente, documento, numero_documento, responsavel = dados
    
    relatorioGerado = diretorioBusca + 'Relatorio_erros_' + numero_documento + '.txt'
    
    with open( relatorioGerado, 'w' ) as file:
        
        relatorio = rel_erro.format( nome_cliente , documento , numero_documento , responsavel , json.dumps( ler_json( diretorioBusca ), indent=4 ) )
        
        

        file.write(relatorio)
        
def ler_json(diretorio):
    with open(diretorio +  'arquivo_erro.json', 'r' ) as fjs:
        data = json.load( fjs )
        return data
        
