import os

def buscaArquivosShp(diretorioBusca):
    arquivos = []
    for r, d, f in os.walk(diretorioBusca):
        for file in f:
            if '.shp' in file:
                arquivos.append( os.path.join( r , file ))
    return arquivos
